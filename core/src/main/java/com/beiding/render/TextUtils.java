package com.beiding.render;

import java.util.Collection;
import java.util.Iterator;

public class TextUtils {

    public static String join(Collection collection, String on) {
        StringBuilder builder = new StringBuilder();

        Iterator iterator = collection.iterator();

        if (iterator.hasNext()) {
            builder.append(iterator.next());
            while (iterator.hasNext()) {
                builder.append(on);
                builder.append(iterator.next());
            }
        }

        return builder.toString();
    }

}
