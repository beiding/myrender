package com.beiding.render;

import java.util.List;

public interface RenderFilterHolder {
    List<RenderFilter> getFilters();
    void addFilter(RenderFilter filter);
}
