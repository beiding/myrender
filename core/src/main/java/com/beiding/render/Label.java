package com.beiding.render;

import java.util.Map;

//通用标签
public class Label {

    //临时占位符
    String holder;

    String name;

    String type;

    TemplateImpl template;

    String body;

    //有序的
    int sort = 0;

    //指令
    Map<String, String> commands;

    //区块中向引用的模板传递的属性
    Map<String, String> attributes;

    Map<String, String> handlers;


}
