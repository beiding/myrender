package com.beiding.render;

//资源搜索器
public interface ResourceFinder {

    //根据资源坐标寻找资源
    Resource findResource(String relative, String path);

    default Resource findResource(String path) {
        return findResource("/", path);
    }

}
