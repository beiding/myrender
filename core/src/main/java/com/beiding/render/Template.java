package com.beiding.render;

public interface Template {
    default String render(Object d) {
        return render(d, coordinate -> new Box() {
            @Override
            public void write(String content) {
            }

            @Override
            public boolean exist() {
                return false;
            }

            @Override
            public boolean canWrite() {
                return true;
            }

            @Override
            public boolean delete() {
                return false;
            }

            @Override
            public String read() {
                return null;
            }
        });
    }

    String render(Object d, BoxManager outCreator);
}
