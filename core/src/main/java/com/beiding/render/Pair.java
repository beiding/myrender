package com.beiding.render;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Pair {
    String holder;

    List<Pair> toRootInners;

    BlockStart blockStart;
    BlockEnd blockEnd;

    //body的获取
    String body;

    int hierarchy;

    @Override
    public String toString() {
        return blockStart.start + "->" + blockEnd.end;
    }


    static class BlockStart extends BlockTag {

        boolean noBody = false;

        Map<String, String> commands = new HashMap<>();

        Map<String, String> attributes = new HashMap<>();

        //方法映射
        Map<String, String> handlers = new HashMap<>();

    }


    static class BlockEnd extends BlockTag {
    }


    static class BlockTag {

        //开始坐标
        int start;

        //结束坐标
        int end;

    }

}


