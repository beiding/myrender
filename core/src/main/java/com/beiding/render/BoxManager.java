package com.beiding.render;

public interface BoxManager {

    //根据坐标创建一个输出
    Box create(String expression);

    default boolean canWrite(String expression) {
        return create(expression).canWrite();
    }

    default boolean delete(String expression) {
        return create(expression).delete();
    }

    default boolean exist(String expression) {
        return create(expression).exist();
    }

    default String read(String expression) {
        return create(expression).read();
    }

}
