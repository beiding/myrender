package com.beiding.render;

import java.util.Map;

//显然时的上下文上下文
public class RenderContext extends TransmissibleMap {

    protected TemplateImpl template;

    protected Map<String, String> commands;

    protected BoxManager outCreator;

    public RenderContext(Map parent, TemplateImpl template, Map<String, String> commands, BoxManager outCreator) {
        super(parent);
        this.template = template;
        this.commands = commands;
        this.outCreator = outCreator;
        put("parent", parent);
    }


    public Map<String, String> getCommands() {
        return commands;
    }

    public Handler findHandler(String r, String p) {
        return template.getRenderTime().getHandlerFinder().findHandler(r, p);
    }

    public Map<String, Object> findPluginBeans(String r, String p) {
        return template.getRenderTime().getPluginFinder().findPluginBeans(r, p);
    }

    public Map<String, Object> findPluginBeans(String s) {
        return template.getRenderTime().getPluginFinder().findPluginBeans(s);
    }

    public Resource findResource(String r, String p) {
        return template.getRenderTime().getResourceFinder().findResource(r, p);
    }

    public Resource findResource(String p) {
        return template.getRenderTime().getResourceFinder().findResource(p);
    }

    public Handler findHandler(String p) {
        return template.getRenderTime().getHandlerFinder().findHandler(p);
    }

    public Template findTemplate(String r, String p) {
        return template.getRenderTime().getTemplateFinder().findTemplate(r, p);
    }

    public Template findTemplate(String p) {
        return template.getRenderTime().getTemplateFinder().findTemplate(p);
    }

    public static String relativeToAbsolute(String relative, String path) {
        return CoordinateUtils.relativeToAbsolute(relative, path);
    }

    @Override
    public String toString() {
        return "[Render Context]";
    }

}
