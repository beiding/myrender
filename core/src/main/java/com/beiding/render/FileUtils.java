package com.beiding.render;

import java.io.*;

public class FileUtils {

    public static InputStream getClassPathResource(String path) {
        return FileUtils.class.getClassLoader().getResourceAsStream(path);
    }

    //将字符串写入到文件中
    public static void writeStringToFile(String s, File file, boolean replaceIfExist) throws IOException {

        File parentFile = file.getParentFile();

        //自动创建父级路径
        if (!parentFile.exists()) {
            if (!parentFile.mkdirs()) {
                throw new IOException("文件目录创建失败:" + parentFile.getAbsolutePath());
            }
        }

        boolean d = true;

        if (file.exists()) {
            if (replaceIfExist) {
                //删除文件后重建文件
                if (!file.delete()) {
                    throw new IOException("文件删除失败:" + file.getAbsolutePath());
                }
                if (!file.createNewFile()) {
                    throw new IOException("文件创建失败:" + file.getAbsolutePath());
                }
            } else {
                d = false;
            }
        } else {
            if (!file.createNewFile()) {
                throw new IOException("文件创建失败:" + file.getAbsolutePath());
            }
        }

        //写入文本后关流
        if (d) {
            FileWriter writer = new FileWriter(file);
            writer.write(s);
            writer.close();
        }
    }

    //读取类路径下文件
    public static String readClassPathResourceAsString(String path) throws IOException {
        InputStream classPathResource = getClassPathResource(path);
        String s = readInputStreamAsString(classPathResource);
        classPathResource.close();
        return s;
    }

    public static String readInputStreamAsString(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        copy(inputStream, byteArrayOutputStream);
        return new String(byteArrayOutputStream.toByteArray());

    }

    public static String readFileAsString(File file) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        InputStream inputStream = new FileInputStream(file);
        copy(inputStream, byteArrayOutputStream);
        inputStream.close();
        return new String(byteArrayOutputStream.toByteArray());

    }

    public static void copy(File from, File to) {
        try {

            FileInputStream inputStream = new FileInputStream(from);
            FileOutputStream outputStream = new FileOutputStream(to);
            copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {

        byte[] buff = new byte[1024];

        int len;
        while ((len = inputStream.read(buff)) != -1) {
            outputStream.write(buff, 0, len);
        }

    }


}
