package com.beiding.render;

import java.util.Map;

class RenderHelp {

    static class RenderResult {
        //private boolean change;
        String text;
    }

    static void handleIndent(RenderResult rr,This _this) {
        String ofs = _this.getCommands().get("indent");
        ofs = (String) ScriptUtils.parseHolder(_this, ofs);
        if (ofs != null) {
            int indent = 0;
            try {
                indent = Integer.valueOf(ofs);
            } catch (Exception ignore) {
            }
            rr.text = ContentUtils.indent(rr.text, indent, false, 0);
        }
    }

    static void preHandleCommands(Map<String, String> commands) {

        String log = commands.get("log");

        if (log != null) {
            //自带移除行
            commands.put("removeLine", "e");
          //  commands.put("gi", "0");
        }

        //TODO  简短指令

        String _ctrl = commands.get("ctrl");
        if (_ctrl != null) {
            commands.put("controller", _ctrl);
        }

        String _b = commands.get("s");
        if (_b != null) {
            commands.put("scriptBefore", _b);
        }

        String _hB = commands.get("h");
        if (_hB != null) {
            commands.put("handlerBefore", _hB);
        }

        String _v = commands.get("val");
        if (_v != null) {
            commands.put("values", _v);
        }

        String _o = commands.get("o");

        if (_o != null) {
            commands.put("out", _o);
        }

        String rL = commands.get("rL");

        if (rL != null) {
            commands.put("removeLine", rL);
        }

        String _oH = commands.get("out");

        if (_oH != null) {
            commands.put("out", _oH);
            commands.put("removeLine", "e");
        }

        String _p = commands.get("p");
        if (_p != null) {
            commands.put("plugins", _p);
        }

        String _g = commands.get("g");
        if (_g != null) {
            commands.put("gap", _g);
        }

        String _d = commands.get("i");
        if (_d != null) {
            commands.put("indent", _d);
        }

        String _gd = commands.get("gi");

        if (_gd != null) {
            commands.put("gap-indent", _gd);
        }

        String _gH = commands.get("gH");
        if (_gH != null) {
            commands.put("gapHead", _gH);
        }

        String _gT = commands.get("gT");
        if (_gT != null) {
            commands.put("gapTail", _gT);
        }


        String _c = commands.get("c");
        if (_c != null) {
            commands.put("call", _c);
        }

        //TODO

        String gapIndent = commands.get("gap-indent");

        if (gapIndent != null) {
            String[] split = gapIndent.split(",");
            if (split.length >= 3) {
                commands.put("gapHead", split[0]);
                commands.put("gapTail", split[1]);
                commands.put("indent", split[2]);
            } else if (split.length == 1) {
                commands.put("gap", split[0]);
                commands.put("indent", split[0]);
            } else if (split.length == 0) {
                commands.put("gap", "0");
                commands.put("indent", "0");
            } else {
                commands.put("gap", split[0]);
                commands.put("indent", split[1]);
            }

        }

        String as = commands.get("as");

        if (as != null) {
            commands.put("as", as);
            commands.put("removeLine", "e");
        }

        String gap = commands.get("gap");

        if (gap != null) {

            String[] split = gap.split(",");

            if (split.length == 2) {
                if (!commands.containsKey("gapHead")) {
                    commands.put("gapHead", split[0]);
                }
                if (!commands.containsKey("gapTail")) {
                    commands.put("gapTail", split[1]);
                }
            } else {
                if (!commands.containsKey("gapHead")) {
                    commands.put("gapHead", gap);
                }
                if (!commands.containsKey("gapTail")) {
                    commands.put("gapTail", gap);
                }
            }
        }

        if (commands.containsKey("gapHead")) {
            String t = commands.get("gapHead");
            if (t == null) {
                t = "";
            }
            commands.put("gapHead", t);
        }

        if (commands.containsKey("gapTail")) {
            String t = commands.get("gapTail");
            if (t == null) {
                t = "";
            }
            commands.put("gapTail", t);
        }

        //使hide和removeLine失效
        if (commands.containsKey("show")) {
            commands.remove("hide");
            commands.remove("removeLine");
        }

        //TODO 支持自定义的命令拓展操作

    }

}
