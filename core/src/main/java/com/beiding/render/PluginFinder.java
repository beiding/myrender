package com.beiding.render;

import java.util.Map;

public interface PluginFinder {

    Map<String, Object> findPluginBeans(String relative, String plugins);

    default Map<String, Object> findPluginBeans(String plugins) {
        return findPluginBeans("/", plugins);
    }


}
