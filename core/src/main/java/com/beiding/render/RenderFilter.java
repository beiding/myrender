package com.beiding.render;

/*
 *@Author 丁常磊
 *@Date 2020/7/4 0:15
 *@Description
 * 渲染拦截器,可以在指令处理前,指令处理后等多个时期进行处理
 */
public interface RenderFilter {



    void filter(This _this, NextFilterHolder nextFilterHolder);

    default int order() {
        return 0;
    }

}
