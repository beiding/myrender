package com.beiding.render;

import java.util.List;

/*
 *  渲染运行时
 *  提供渲染时所需要的模板找寻器,插件查询器等
 *
 *  渲染运行时封装了所有渲染过程中可能会涉及到的所有对象,例如,识别出需要调用外部插件时或者需要调用外部模板时就可以按需获取到对应的外部插件或者模板
 *
 *  运行时也封装了一个渲染拦截器,可以在拦截器中拦截默认的渲染行为,加入自己的指令使渲染更加个性化
 *
 *
 */
public class RenderTime {

    private TemplateFinder templateFinder;

    private PluginFinder pluginFinder;

    private HandlerFinder handlerFinder;

    private ResourceFinder resourceFinder;

    private List<RenderFilter> filters;

    public RenderTime(TemplateFinder templateFinder, PluginFinder pluginFinder, HandlerFinder handlerFinder, ResourceFinder resourceFinder, List<RenderFilter> filters) {
        this.templateFinder = templateFinder;
        this.pluginFinder = pluginFinder;
        this.handlerFinder = handlerFinder;
        this.resourceFinder = resourceFinder;
        this.filters = filters;
    }

    public void render(This controllerContext) {
        NextFilterHolder holder = new NextFilterHolder(-1, filters, controllerContext);
        holder.next();
    }

    public List<RenderFilter> getFilters() {
        return filters;
    }

    public TemplateFinder getTemplateFinder() {
        return templateFinder;
    }

    public PluginFinder getPluginFinder() {
        return pluginFinder;
    }

    public ResourceFinder getResourceFinder() {
        return resourceFinder;
    }


    public HandlerFinder getHandlerFinder() {
        return handlerFinder;
    }

/*    RenderTime copyFor(Map<String, TemplateImpl> innerTpMap) {
        return new RenderTime(new InnerAbleTemplateFinder(this.templateFinder, innerTpMap), pluginFinder, handlerFinder, resourceFinder, filters);
    }*/

}
