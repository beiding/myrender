package com.beiding.render;

public interface Box {

    //将文本内容直接写入
    void write(String content);

    //是否已经存在了
    boolean exist();

    boolean canWrite();

    boolean delete();

    //读取内容
    String read();

}
