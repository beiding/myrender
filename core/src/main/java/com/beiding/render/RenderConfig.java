package com.beiding.render;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RenderConfig {
    private static Logger logger = new Logger() {
        private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        @Override
        public void log(String code, String log) {
            if (log != null && !log.trim().equals("")) {
                System.out.println(dateFormat.format(new Date()) + " - " + code + " :");
                System.out.println(log);
            }
        }
    };

    public static void setLogger(Logger logger) {
        RenderConfig.logger = logger;
    }

    public static Logger getLogger() {
        return logger;
    }
}
