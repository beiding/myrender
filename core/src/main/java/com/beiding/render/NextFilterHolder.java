package com.beiding.render;

import java.util.List;

/*
 *@Author 丁常磊
 *@Date 2020/7/4 22:18
 *@Description
 *  下一拦截器持有
 *
 *  调用handle
 *
 */
public class NextFilterHolder {

    //令牌
    private boolean token = true;

    //游标
    private int index;

    //渲染器
    private List<RenderFilter> filters;

    private This renderControllerContext;


    public NextFilterHolder(int index, List<RenderFilter> filters, This renderControllerContext) {
        this.index = index;
        this.filters = filters;
        this.renderControllerContext = renderControllerContext;
    }

    //触发下一Filter 的执行
    public void next() {

        if (!token) {
            return;
        }

        //令牌只能使用一次
        token = false;

        //游标后移
        index++;

        if (index > filters.size()) {
            return;
        }

        RenderFilter renderFilter = filters.get(index);
        NextFilterHolder nextFilterHolder = new NextFilterHolder(index, filters, renderControllerContext);
        renderFilter.filter(renderControllerContext, nextFilterHolder);
    }

}
