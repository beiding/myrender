package com.beiding.render;

import java.util.HashMap;
import java.util.Map;

public class Commands {

    /*
        内置对象
        $b boxManager

     */

    public static final Map<String, String> commandsDescription = new HashMap<String, String>() {
        {
            put("log", "可以将bk渲染的结果作为日志进行打印,而不再输出");

            put("if", "bk和rd节点渲染的条件");

            put("elif", "bk和rd节点渲染时的条件,跟在if指令后");

            put("else", "bk和rd节点渲染时条件,跟在if或elif指令后");

            put("for", "bk和rd节点的遍历渲染");

            put("join", "辅助于for指令,该指令可以将for渲染的多个结果使用join给出的字符串进行分割");

            put("switch", "bk节点上判断一个值");

            put("case", "bk或rd节点上对父节点上的switch给定值进行判断");

            put("default", "bk或rd节点上对同级节点都不满足时的默认操作");

            put("controller", "bk和tp节点自定义的渲染控制器,简写'ctrl'");

            put("scriptBefore", "bk和tp节点渲染前执行的脚本,简写's'");
            put("handlerBefore", "bk和tp节点渲染前执行的Handler,简写'h'");

            put("hide", "将渲染的结果隐藏,当前位置不再输出");

            put("removeLine", "bk和rd节点在渲染后,是否移除其所在的行,简写'rL',两个参数可选,empty(当前行不含其它内容时移除该行,默认,简写'e');any(无论任何情况都清空该行,简写'a')");

            put("show", "可使removeLine和hide指令失效");

            put("values", "bk和tp节点渲染前的默认值,简写'val'");

            put("plugins", "tp节点渲染时引入的外置插件,简写'p'");

            put("tp", "将bk节点提取为内置模板,供其他位置调用");
            put("call", "调用其他模板,简写'c'");

            put("as", "将渲染的结果输出至变量,自带removeLine属性");

            put("out", "bk和tp节点渲染后,使用OutCreator创建一个输出,并将渲染的结果输出至目标位置,自带removeLine属性,简写'o'");

            put("gap", "bk和tp节点渲染时上下间距的行数,简写'g'");
            put("gapHead", "bk和tp节点渲染时上间距的行数,简写'gH'");
            put("gapTail", "bk和tp节点渲染时下间距的行数,简写'gT'");

            put("indent", "bk和tp节点渲染时相对左侧缩进的空格数,简写'i'");
            put("gap-indent", "bk和tp节点渲染时上下间隔行数和相对左侧缩进空格数,两数以英文逗号分隔,简写'gi'");

        }
    };


}
