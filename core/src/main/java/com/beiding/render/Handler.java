package com.beiding.render;

//在渲染之前对入参进行预处理,例如: 对于render而言,传入了 name 参数 可以将其添加些前后缀等再进行渲染
public interface Handler {

    String name();

    Object handle(Object... params);

}
