package com.beiding.render;


import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

public class ClassUtils {

    public static Set<Class> scan(File file) {
        try {
            Set<Class> r = new HashSet<>();
            MyClassLoader myClassLoader = new MyClassLoader(file.toURI().toURL());
            Set<String> set = ClasspathUtils.scanClassName(file);
            for (String s : set) {
                try {
                    Class<?> aClass = myClassLoader.loadClass(s);
                    r.add(aClass);
                } catch (Exception ignore) {

                }
            }
            return r;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static class MyClassLoader extends URLClassLoader {
        public MyClassLoader(URL url) {
            super(new URL[]{url});
        }

        public Class define(byte[] bs) {
            try {
                return defineClass(null, bs, 0, bs.length);
            } catch (Exception e) {
                return null;
            }
        }
    }


}
