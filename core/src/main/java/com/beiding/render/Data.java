package com.beiding.render;

import java.util.HashMap;

//数据快速构建器
public class Data extends HashMap<String, Object> {

    public static Data newSet() {
        return new Data();
    }

    public static Data newSet(String key, Object value) {
        Data dt = new Data();
        dt.put(key, value);
        return dt;
    }

    public Data set(String key, Object value) {
        this.put(key, value);
        return this;
    }

}
