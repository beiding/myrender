package com.beiding.render;

import java.util.HashMap;
import java.util.Map;

public class TransmissibleMap extends HashMap {

    //绑定一个父元素
    public final Map parent;

    public TransmissibleMap(Map parent) {
        this.parent = parent;
    }

    @Override
    public Object get(Object key) {
        Object o = super.get(key);
        if (o == null) {
            if (parent != null) {
                return parent.get(key);
            }
        }
        return o;
    }

    @Override
    public boolean containsKey(Object key) {
        if (key instanceof String) {
            if (!((String) key).contains(".")) {
                return true;
            }
        }
        return super.containsKey(key);
    }

}
