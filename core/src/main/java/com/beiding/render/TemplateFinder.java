package com.beiding.render;

//模板搜索器
public interface TemplateFinder {

    /**
     * 相对路径寻找模板
     * @param relative 相对的路径
     * @param expression 模板表达式
     * @return
     */
    Template findTemplate(String relative, String expression);

    /**
     * 相对根路径寻找模板
     * @param expression 模板表达式
     * @return
     */
    default Template findTemplate(String expression) {
        return findTemplate("/", expression);
    }

}
