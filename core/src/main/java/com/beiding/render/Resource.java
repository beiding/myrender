package com.beiding.render;

import java.io.IOException;

public interface Resource {

    //资源名称
   /* //资源名称
    String getName();*/
/*

    //资源坐标
    String getCoordinate();
*/

    //载入
    byte[] load() throws IOException;

    //读取资源数据为字符串
    default String read() throws IOException {
        return new String(load());
    }

    //资源是否依然有效
    boolean isEffective();

}
