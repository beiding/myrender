package com.beiding.render;

public interface HandlerFinder {
    Handler findHandler(String relative, String path);
    default Handler findHandler(String path) {
        return findHandler("/", path);
    }
}
