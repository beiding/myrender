package com.beiding.demo;

import com.beiding.render.Data;
import com.beiding.render.Template;
import com.beiding.render.TemplateCompiler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class Test1 {


    public static void main(String[] args) throws IOException {


        TemplateCompiler templateCompiler = new TemplateCompiler();

        //编译一个helloword 的文本
        Template template = templateCompiler.compile(loadText("helloword"), "/");

        //进行渲染操作
        System.out.println(
                template.render(
                        Data.newSet()
                )
        );


        //bk区块使用说明
        template = templateCompiler.compile(loadText("bk"), "/");

        //进行渲染操作
        System.out.println(
                template.render(
                        Data.newSet()
                                .set("names", Arrays.asList("小明", "小亮", "小刚"))
                )
        );


    }


    private static String loadText(String name) throws IOException {

        InputStream resourceAsStream = Test1.class.getResourceAsStream("/com/beiding/demo/templates/" + name + ".xml");

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int len;

        byte[] buff = new byte[1024];

        while ((len = resourceAsStream.read(buff)) != -1) {
            byteArrayOutputStream.write(buff, 0, len);
        }

        resourceAsStream.close();

        return new String(byteArrayOutputStream.toByteArray());
    }


}
