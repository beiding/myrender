package com.beiding.cprender;

public interface CpPlugin {
    default String name() {
        String simpleName = this.getClass().getSimpleName();
        //handler名称默认规则是将类名的第一个字母该为小写
        simpleName = simpleName.substring(0, 1).toLowerCase() + simpleName.substring(1);
        return simpleName;
    }

}
