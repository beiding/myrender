package com.beiding.cprender;

/*
    类下资源导出器
 */
public interface CpExporter {

    //如果返回null则返回全部,如果给定则导出给定的
    default Class[] export() {
        return null;
    }

}
