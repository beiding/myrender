package com.beiding.cprender;

import java.net.URL;
import java.net.URLClassLoader;

/*
 *@Author 丁常磊
 *@Date 2020/8/7 23:58
 *@Description
 *
 */
public class URLFinder {

    private URLClassLoader urlClassLoader;

    public URLFinder(URL... url) {
        this.urlClassLoader = new URLClassLoader(url, null);
    }

    public URL find(String name) {
        return urlClassLoader.getResource(name);
    }



}
