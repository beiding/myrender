package com.beiding.cprender;


import com.beiding.render.Resource;

import java.io.IOException;
import java.net.URL;

public class CpResource implements Resource {

    private URL url;

    private byte[] content;

    public CpResource(URL url) {
        this.url = url;
    }

    @Override
    public byte[] load() throws IOException {
        if (content == null) {
            content = ResourceUtils.readByte(url.openConnection().getInputStream());
        }
        return content;
    }

    @Override
    public boolean isEffective() {
        return true;
    }
}
