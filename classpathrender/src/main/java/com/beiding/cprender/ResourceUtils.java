package com.beiding.cprender;

import java.io.*;
import java.net.URL;

/*
 *@Author 丁常磊
 *@Date 2020/8/7 22:26
 *@Description
 * 通用读写单元,支持字节流字符流,文件等读写
 */
public class ResourceUtils {

    public static String read(Object input) {
        try {
            return read0(input);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public static Reader toReader(Object input) throws IOException {
        if (input instanceof File) {
            try {
                FileInputStream fileInputStream = new FileInputStream((File) input);
                return toReader(fileInputStream);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else if (input instanceof InputStream) {
            return new InputStreamReader((InputStream) input);
        } else if (input instanceof URL) {
            return toReader(((URL) input).openStream());
        } else {
            throw new RuntimeException("不支持的输入:" + input);
        }
    }

    private static String read0(Object input) throws IOException {
        Reader reader = toReader(input);
        char[] buff = new char[512];
        int len;
        StringBuilder builder = new StringBuilder();
        while ((len = reader.read(buff)) != -1) {
            builder.append(buff, 0, len);
        }
        return builder.toString();
    }

    public static byte[] readByte(Object input) throws IOException {
        if (input instanceof File) {
            try {
                FileInputStream fileInputStream = new FileInputStream((File) input);
                byte[] s = readByte(fileInputStream);
                fileInputStream.close();
                return s;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else if (input instanceof Reader) {
            char[] buff = new char[512];
            int len;
            StringBuilder builder = new StringBuilder();
            Reader reader = (Reader) input;
            while ((len = reader.read(buff)) != -1) {
                builder.append(buff, 0, len);
            }
            return builder.toString().getBytes();
        } else if (input instanceof InputStream) {
            byte[] buff = new byte[1024];
            int len;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            InputStream in = (InputStream) input;
            while ((len = in.read(buff)) != -1) {
                byteArrayOutputStream.write(buff, 0, len);
            }
            return byteArrayOutputStream.toByteArray();
        } else if (input instanceof URL) {
            return readByte(((URL) input).openStream());
        } else {
            throw new RuntimeException("不支持的输入");
        }
    }

    public static void write(String content, Object output) {
        try {
            write0(content, output);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void write0(String content, Object output) throws IOException {
        if (output instanceof File) {
            try {
                File parentFile = ((File) output).getParentFile();
                if (!parentFile.exists()) {
                    parentFile.mkdirs();
                }
                FileOutputStream stream = new FileOutputStream((File) output);
                write0(content, stream);
                stream.flush();
                stream.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else if (output instanceof Writer) {
            ((Writer) output).write(content);
            ((Writer) output).flush();
        } else if (output instanceof OutputStream) {
            write0(content, new OutputStreamWriter((OutputStream) output));
        } else {
            throw new RuntimeException("不支持的输入");
        }
    }


    //


}
