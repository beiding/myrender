package com.beiding.cprender;

import com.beiding.render.BoxManager;
import com.beiding.render.Template;

/*
    把模板,Handler以及插件都封装成类
    通过这些类进而实现模板的继承,模板之间的相互引用,分包管理等效果
 */
public abstract class CpTemplate implements Template {

    protected Template target;


    //模板坐标
    public abstract String name();

    void setTarget(Template target) {
        this.target = target;
    }


    @Override
    public String render(Object d, BoxManager outCreator) {
        return target.render(d, outCreator);
    }
}
