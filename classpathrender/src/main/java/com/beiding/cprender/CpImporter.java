package com.beiding.cprender;

import java.util.Map;

/*
    类下资源导入器
    将Exporter导入进来使用,key是命名空间,调用时使用key作为前缀进行调用


 */
public interface CpImporter {

    Map<String, Class<? extends CpExporter>> imports();

}
