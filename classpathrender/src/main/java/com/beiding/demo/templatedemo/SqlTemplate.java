package com.beiding.demo.templatedemo;

import com.beiding.cprender.CpTemplate;
import com.beiding.render.Data;

public abstract class SqlTemplate extends CpTemplate {

    public String sql(String sqlCode, Data data) {
        data.set("__" + sqlCode, true);
        data.set("_" + GlobalContext.getDbType(), true);
        return this.render(data).replace("\n", " ");
    }

}
