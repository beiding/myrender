package com.beiding.demo;

import com.beiding.cprender.F;
import com.beiding.demo.templatedemo.TestSql;
import com.beiding.render.Data;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {

        //获取一个Sql模板
        TestSql test = F.get(TestSql.class);

        //利用sql模板将数据进行渲染出来
        System.out.println(test.sql("a",
                Data.newSet()
                        .set("f", 100)
                        .set("t", 200)
                        .set("names", Arrays.asList("丁", "王"))
        ));

    }
}
