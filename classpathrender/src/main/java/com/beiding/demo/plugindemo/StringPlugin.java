package com.beiding.demo.plugindemo;

import com.beiding.cprender.CpPlugin;

/*
    这个插件会被自动装载到模板中,因此不需要声明就可以在模板中直接调用
 */
public class StringPlugin implements CpPlugin {

    @Override
    public String name() {
        return "stringPlugin";
    }

    public String format(String sql) {
        return sql.replace("\r\n", "\n");
    }

}
